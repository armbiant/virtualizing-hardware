#### classes[1] = "Virtualizing hardware"

##### Análise de Sequências Biológicas 2022-2023

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Hardware 101

* &shy;<!-- .element: class="fragment" -->Typical x86 PC case internals
  * &shy;<!-- .element: class="fragment" -->Motherboard
  * &shy;<!-- .element: class="fragment" -->CPU/APU
  * &shy;<!-- .element: class="fragment" -->GPU
  * &shy;<!-- .element: class="fragment" -->RAM
  * &shy;<!-- .element: class="fragment" -->Storage
  * &shy;<!-- .element: class="fragment" -->PSU
  * &shy;<!-- .element: class="fragment" -->Network card(s)
  * &shy;<!-- .element: class="fragment" -->Other I/O ports

<div style="float: right" class="fragment">

![PC case](assets/pc_case.jpg)

</div>

---

### Take a few minutes to look at one

---

### Hardware meets software

* &shy;<!-- .element: class="fragment" -->A Virtual Machine (VM)
* &shy;<!-- .element: class="fragment" -->Is "a PC inside your PC"
* &shy;<!-- .element: class="fragment" -->The "real" PC is the *Host* 
* &shy;<!-- .element: class="fragment" -->The "virtual" PC is the *Guest* 
* &shy;<!-- .element: class="fragment" -->Most of the *guest* "hardware" is software defined
* &shy;<!-- .element: class="fragment" -->Some parts are directly passed from *host* to *guest* 

&shy;<!-- .element: class="fragment" -->![Hologram keyboard](assets/pc_hologram.png)

---

### What is this for?

* &shy;<!-- .element: class="fragment" -->Running an **unmodified** *guest* OS on the *host* system
* &shy;<!-- .element: class="fragment" -->Running multiple OSes in the same machine
* &shy;<!-- .element: class="fragment" -->Spawning "new" machines on demand using a single physical machine


---

### VM advantages over physical systems

* &shy;<!-- .element: class="fragment" -->Spawned on demand
* &shy;<!-- .element: class="fragment" -->Portable (Easy to move between physical machines)
* &shy;<!-- .element: class="fragment" -->Easy to copy
* &shy;<!-- .element: class="fragment" -->Snapshot support
* &shy;<!-- .element: class="fragment" -->Scalable
* &shy;<!-- .element: class="fragment" -->Security benefits

<div style="float: right" class="fragment">

![Pony upvote](assets/pony_upvote.png)

</div>

---

### VM limitations

* &shy;<!-- .element: class="fragment" -->*Host* and *guest* code must share the same architecture
  * &shy;<!-- .element: class="fragment" -->Unless you use emulation
* &shy;<!-- .element: class="fragment" -->Has some performance hit
* &shy;<!-- .element: class="fragment" -->No GPU acceleration
  * &shy;<!-- .element: class="fragment" -->Experimental support for GPU pass-through
* &shy;<!-- .element: class="fragment" -->Resource sharing

<div style="float: right" class="fragment">

![Pony downvote](assets/pony_downvote.png)

</div>

---

### *Host* system

* x86_64 architecture  <!-- .element: class="fragment" data-fragment-index="1" -->
* MS Windows 10 (64bit) OS <!-- .element: class="fragment" data-fragment-index="2" -->
* 8Gb RAM <!-- .element: class="fragment" data-fragment-index="3" -->
* Large SSD/HDD <!-- .element: class="fragment" data-fragment-index="4" -->
* Defines resource sharing <!-- .element: class="fragment" data-fragment-index="5" -->
* VirtualBox 6.X <!-- .element: class="fragment" data-fragment-index="6" -->

|||

### VirtualBox

* Virtualization software <!-- .element: class="fragment" data-fragment-index="1" -->
* Mostly open source <!-- .element: class="fragment" data-fragment-index="2" -->
  * Some optional proprietary parts <!-- .element: class="fragment" data-fragment-index="2" -->
* Very easy to use <!-- .element: class="fragment" data-fragment-index="3" -->
* GUI and CLI interfaces <!-- .element: class="fragment" data-fragment-index="4" -->
* Alternatives <!-- .element: class="fragment" data-fragment-index="5" -->
  * VMware <!-- .element: class="fragment" data-fragment-index="5" -->
  * KVM <!-- .element: class="fragment" data-fragment-index="5" -->
  * XEN <!-- .element: class="fragment" data-fragment-index="5" -->
  * Hyper-V <!-- .element: class="fragment" data-fragment-index="5" -->

---

### *Guest* system

* &shy;<!-- .element: class="fragment" -->x86_64 architecture
* &shy;<!-- .element: class="fragment" -->[[X]](https://xubuntu.org/)[Ubuntu](https://www.ubuntu.com/) (64 bit) OS
* &shy;<!-- .element: class="fragment" -->4Gb RAM
* &shy;<!-- .element: class="fragment" -->Small storage device
* &shy;<!-- .element: class="fragment" -->Can seamlessly scale resources

|||

### What is Ubuntu?

* &shy;<!-- .element: class="fragment" -->GNU/Linux based OS
* &shy;<!-- .element: class="fragment" -->Designed to be easy to use
  * &shy;<!-- .element: class="fragment" -->"Newbie friendly"
* &shy;<!-- .element: class="fragment" -->User is in control
  * &shy;<!-- .element: class="fragment" -->Good starting point for Bioinformatics
* &shy;<!-- .element: class="fragment" -->Code is [Open Source](https://opensource.com/resources/what-open-source)

&shy;<!-- .element: class="fragment" -->[![Ubuntu Logo](assets/ubuntu.png)](https://www.ubuntu.com/)

---

### Create your own VM

* &shy;<!-- .element: class="fragment" -->HDD - 60Gb
* &shy;<!-- .element: class="fragment" -->RAM - 4Gb

&shy;<!-- .element: class="fragment" -->![Virtual machines](assets/virtualization.png)

|||

### A word about GNU/Linux systems

* &shy;<!-- .element: class="fragment" -->Follow the [Unix Philosophy](https://www.linuxtopia.org/online_books/programming_books/art_of_unix_programming/ch01s06.html)
* &shy;<!-- .element: class="fragment" -->*Write programs that do one thing and do it well. Write programs to work together. Write programs to handle text streams, because that is a universal interface* 
* &shy;<!-- .element: class="fragment" -->Everything is a file (descriptor)!

&shy;<!-- .element: class="fragment" -->![It's a Unix system meme](assets/its-a-unix-system-I-know-this.jpg)

---

### Goals for today

* Create a VM
* Install Ubuntu
  * [Download link](http://mirrors.up.pt/pub/ubuntu-releases/22.04/ubuntu-22.04.1-desktop-amd64.iso)
  * [Lightweight alternative](http://ftp.free.fr/mirrors/ftp.xubuntu.com/releases/22.04/release/xubuntu-22.04.1-desktop-amd64.iso)

---

## Now, we go live!

---

### References

* [What is a Virtual Machine?](https://www.oracle.com/sg/cloud/compute/virtual-machines/what-is-virtual-machine/)
* [Why, When, and How To Use a Virtual Machine - The Linux foundation](https://www.linux.com/learn/why-when-and-how-use-virtual-machine)
* [What is Virtualization - VMWare](https://www.vmware.com/solutions/virtualization.html)
* [Ubuntu](https://www.ubuntu.com/)
* [The Unix Philosophy](https://www.linuxtopia.org/online_books/programming_books/art_of_unix_programming/ch01s06.html)
* [Everything is a file (descriptor)](https://lwn.net/Articles/411845/)
